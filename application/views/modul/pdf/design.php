<html>

<head>
    <style>
        body {
            font-family: arial;
        }

        @page {
            margin: 25px;
        }

        .table1,
            {
            width: 100%;
            border-collapse: collapse;
        }

        .bold {
            font-weight: bold;
        }

        .right {
            text-align: right;
        }

        .center {
            text-align: center;
        }

        .f8 {
            font-size: 8px;
        }

        .f10 {
            font-size: 10px;
        }

        .f12 {
            font-size: 12px;
        }

        .f14 {
            font-size: 14px;
        }

        .border {
            border: 1px solid #000;
        }

        .no-border {
            border: 0px !important;
        }

        .kotak {
            border: 1px solid #000;
            width: 15px;
            height: 25px;
            padding: 5px;
        }
    </style>
</head>

<body>
    <table border="1" class="table1">
        <tr>
            <td colspan="2"></td>
            <td class="f12 bold center">BORANG AZ 001</td>
        </tr>
        <tr>
            <td><img src="./assets/images/pdf-header.png" width="250" /></td>
            <td width="300">
                <table style="border: 1px solid red; width: 295px;">
                    <tr>
                        <td class="f10 bold center">UNTUK KEGUNAAN PEJABAT SAHAJA</td>
                    </tr>
                    <tr>
                        <td class="f10 border" valign="bottom" style="width: -webkit-fill-available; display: inline-flex;">
                            <span style="margin-top: 13px; margin-right: 12px;">Permohonan :</span>
                            <div class="kotak"></div>
                            <span style="margin-top: 13px; margin-left: 12px; margin-right: 12px;">Baru</span>
                            <div class="kotak"></div>
                            <span style="margin-top: 13px; margin-left: 12px;">Ulangan</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="f10">No. Rujukan :</td>
                    </tr>
                    <tr>
                        <td class="f10 border" style="display: inline-flex;">
                            <?php
                            $a = 1;
                            while ($a <= 16) {
                                echo '<div class="kotak"></div>';
                                $a++;
                            }
                            ?>
                        </td>
                    </tr>
                </table>
            </td>
            <td rowspan="3" width="160">
                <table style="border: 1px dashed red;">
                    <tr>
                        <td style="height: 160px;" class="f12 bold center">GAMBAR BERUKURAN PASSPORT</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="f12"><b>BORANG PERMOHONAN SKIM BANTUAN BAITULMAL MAIWP</b></td>
        </tr>
    </table>
</body>