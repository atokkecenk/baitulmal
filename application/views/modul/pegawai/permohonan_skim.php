﻿<div class="card">
  <div class="card-header p-2">
    <ul class="nav nav-pills pull-right">
      <li class="nav-item">
        <button type="button" class="btn btn-outline-secondary btn-sm"><i class="fas fa-plus"></i> ADD</button>
      </li>
    </ul>
  </div>
  <div class="card-body">
    <?= form_open('#', 'method="post" target="_blank"') ?>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label>PERMOHONAN</label>
          <select name="" class="form-control">
            <option value="">- Select Option -</option>
            <option value="b">Baru</option>
            <option value="u">Ulangan</option>
          </select>
        </div>
        <!-- <div class="form-group">
          <span class="text-info">
            <marquee>on progress..</marquee>
          </span>
        </div> -->
        <div class="form-group">
          <!-- <button type="button" class="btn btn-info"><i class="fas fa-save"></i> Save</button> -->
          <button type="button" onclick="window.open('permohonan-skim-pdf', '_blank')" class="btn btn-warning"><i class="fas fa-print"></i> Pdf</button>
          <style>
            .blink {
              animation: blink-animation 0.8s steps(5, start) infinite;
              -webkit-animation: blink-animation 0.8s steps(5, start) infinite;
            }

            @keyframes blink-animation {
              to {
                visibility: hidden;
              }
            }

            @-webkit-keyframes blink-animation {
              to {
                visibility: hidden;
              }
            }
          </style>
          <span class="blink text-danger">&larr; Let's Go, Try <b>print the Pdf !!</b></span>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>NO. RUJUKAN</label>
          <input type="number" class="form-control" placeholder="No. Rujukan">
        </div>
      </div>
    </div>
    <?= form_close() ?>
  </div>
</div>