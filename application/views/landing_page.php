<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PORTAL E-SEKOLAH BAITULMAL</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="<?= base_url() ?>assets/images/logo.png">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/themes/plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/themes/dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- Added -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/themes/dist/css/myCss.css">
    <style>
        .border {
            border: 1px solid red;
        }

        .bold {
            font-weight: bold;
        }

        .background {
            background-image: url("<?= base_url() ?>assets/images/bangunanIKB.jpg");
            height: 500px;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        .my-card {
            cursor: pointer;
            border-radius: 35px;
            border: 3px solid #467fff;
        }

        .page {
            margin: 0 auto;
            /* margin-top: 2%; */
        }

        .text-selamat {
            font-weight: bold;
            color: #000;
            margin: 0 auto;
            margin-top: 2%;
            margin-bottom: 2%;
        }

        .image-center {
            margin: 0 auto;
        }

        .pdt-15 {
            padding-top: 15px;
        }
    </style>
</head>

<body class="hold-transition  background">
    <div class="col-lg-12">
        <div class="row">
            <h1 class="text-selamat">SELAMAT DATANG KE PORTAL E-SEKOLAH BAITULMAL</h1>
        </div>
    </div>

    <div class="col-lg-12">
        <div class="row">
            <img src="<?= base_url() ?>assets/images/IMG_20210213_182815.png" class="image-center" alt="background-maiwp" width="450">
        </div>
    </div>
    <div class="col-lg-12">
        <?php
        if ($this->session->flashdata('error')) {
            echo '
                                <div class="col-sm-9 alert alert-danger alert-dismissible fade show" role="alert" style="margin: 0 auto;margin-bottom: 15px;">
                                <i class="fas fa-info-circle"></i> Error Message :<br>' . $this->session->flashdata('error') . '
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                            ';
        }
        ?>
    </div>
    <div class="col-lg-12">
        <div class="row page">

            <div class="col-md-4">
                <!-- <div class="card my-card" data-toggle="modal" data-target="#modal-default"> -->
                <div class="card my-card" role="button" data-type="PEGAWAI">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <img src="<?= base_url() ?>assets/images/logomaiwpbaru2019.jpg" height="100">
                            </div>
                            <div class="col-sm-9 pdt-15">
                                <h3 class="bold">DAFTAR MASUK<br>PEG. BAITULMAL</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <!-- <div class="card my-card" data-toggle="modal" data-target="#modal-default"> -->
                <div class="card my-card" role="button" data-type="GURU SEKOLAH">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <img src="<?= base_url() ?>assets/images/muslimah.jpg" height="100">
                            </div>
                            <div class="col-sm-9 pdt-15">
                                <h3 class="bold">DAFTAR MASUK<br>GURU SEKOLAH</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <!-- <div class="card my-card" data-toggle="modal" data-target="#modal-default"> -->
                <div class="card my-card" role="button" data-type="PELAJAR">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <img src="<?= base_url() ?>assets/images/muslim.jpeg" height="100">
                            </div>
                            <div class="col-sm-9 pdt-15">
                                <h3 class="bold">DAFTAR MASUK<br>ASNAF / PELAJAR</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL -->
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><i class="fas fa-sign-in-alt mr-2"></i>Sign In Page</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?= form_open('login', 'method = "post"') ?>
                    <p class="login-box-msg"><span class="text-danger">Sign in</span> to start your session</p>
                    <small class="text-info">Credential is readonly</small>
                    <div class="input-group mb-3">
                        <!-- <div class="input-group-prepend">
                            <span class="input-group-text">
                                Credential
                            </span>
                        </div> -->
                        <input type="text" class="form-control is-valid credential" disabled>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" name="username" class="form-control" placeholder="Username.." required>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" name="password" class="form-control" placeholder="Password.." required>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>

                    <div class="social-auth-links text-center mb-3">
                        <button type="submit" class="btn btn-block btn-primary">
                            SIGN IN
                        </button>
                    </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="<?= base_url() ?>assets/themes/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="<?= base_url() ?>assets/themes/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script>
        $(document).ready(function() {
            $('.my-card').click(function() {
                var modal = $(this).data('type');
                $('#modal-default').modal('show');
                $('.credential').val(modal);
            });
        });
    </script>
</body>

</html>