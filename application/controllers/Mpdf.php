<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mpdf extends MY_Controller
{

    public function index()
    {
        $data = $this->load->view('modul/pdf/mpdf_v');
    }

    public function printPDF()
    {
        $mpdf = new \Mpdf\Mpdf([
            'mode' => 'utf-8',
            // 'format' => 'Letter',
            'format' => [215.9, 300],
            'orientation' => 'P'
        ]);

        $mpdf->SetTitle('Design Pdf - Permohonan SKIM');
        $mpdf->AddPage();

        // background
        $mpdf->Image(FCPATH . "/assets/images/pdf-1.PNG", 3, 4, 210, 0, 'PNG', '', true, false);
        $mpdf->Image(FCPATH . "/assets/images/pdf-2.PNG", 3, 117, 210, 0, 'PNG', '', true, false);
        $mpdf->Image(FCPATH . "/assets/images/pdf-3.PNG", 3.4, 227, 209.6, 0, 'PNG', '', true, false);

        // permohonan
        // kiri ke kanan
        $pm = 1;
        switch ($pm) {
            case '1':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 125, 18, 3, 3, 'PNG', '', true, false);
                break;
            case '2':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 146, 18, 3, 3, 'PNG', '', true, false);
                break;
        }
        ##

        // jenis bantuan 
        // urut dari atas kiri ke kanan, lalu ke bawah, seterusnya
        $jb = 27;
        switch ($jb) {
            case '1':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 15, 61, 3, 3, 'PNG', '', true, false);
                break;
            case '2':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 84, 61, 3, 3, 'PNG', '', true, false);
                break;
            case '3':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 150.2, 61, 3, 3, 'PNG', '', true, false);
                break;
            case '4':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 15, 67.2, 3, 3, 'PNG', '', true, false);
                break;
            case '5':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 84, 67.2, 3, 3, 'PNG', '', true, false);
                break;
            case '6':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 150.2, 67.2, 3, 3, 'PNG', '', true, false);
                break;
            case '7':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 15, 67.2, 3, 3, 'PNG', '', true, false);
                break;
            case '8':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 84, 67.2, 3, 3, 'PNG', '', true, false);
                break;
            case '9':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 150.2, 67.2, 3, 3, 'PNG', '', true, false);
                break;
            case '10':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 15, 74, 3, 3, 'PNG', '', true, false);
                break;
            case '11':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 84, 74, 3, 3, 'PNG', '', true, false);
                break;
            case '12':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 150.2, 74, 3, 3, 'PNG', '', true, false);
                break;
            case '13':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 15, 79.7, 3, 3, 'PNG', '', true, false);
                break;
            case '14':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 84, 79.7, 3, 3, 'PNG', '', true, false);
                break;
            case '15':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 150.2, 79.7, 3, 3, 'PNG', '', true, false);
                break;
            case '16':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 15, 86, 3, 3, 'PNG', '', true, false);
                break;
            case '17':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 84, 86, 3, 3, 'PNG', '', true, false);
                break;
            case '18':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 150.2, 86, 3, 3, 'PNG', '', true, false);
                break;
            case '19':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 15, 92.2, 3, 3, 'PNG', '', true, false);
                break;
            case '20':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 84, 92.2, 3, 3, 'PNG', '', true, false);
                break;
            case '21':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 150.2, 92.2, 3, 3, 'PNG', '', true, false);
                break;
            case '22':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 15, 98.2, 3, 3, 'PNG', '', true, false);
                break;
            case '23':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 84, 98.2, 3, 3, 'PNG', '', true, false);
                break;
            case '24':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 150.2, 98.2, 3, 3, 'PNG', '', true, false);
                break;
            case '25':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 15, 104.5, 3, 3, 'PNG', '', true, false);
                break;
            case '26':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 84, 104.5, 3, 3, 'PNG', '', true, false);
                break;
            case '27':
                $mpdf->Image(FCPATH . "/assets/images/icon/check.png", 150.2, 104.5, 3, 3, 'PNG', '', true, false);
                break;
        }
        ## 

        // jenis bantuan 
        // urut dari atas kiri ke kanan, lalu ke bawah, seterusnya
        $mpdf->SetXY(36, 131);
        $mpdf->SetFont('Arial', '', 10);
        $mpdf->WriteCell(10, 5, chunk_split('123881', 1, '  '), 0, 1, 'L');
        $mpdf->SetXY(65.4, 131);
        $mpdf->WriteCell(10, 5, chunk_split('79', 1, '  '), 0, 1, 'L');
        $mpdf->SetXY(79, 131);
        $mpdf->WriteCell(10, 5, chunk_split('1234', 1, '  '), 0, 1, 'L');
        ##

        $mpdf->SetXY(101, 30);
        $mpdf->SetFont('Arial', '', 10);
        $mpdf->WriteCell(10, 5, chunk_split('12345678910', 1, '  '), 0, 1, 'L');

        $mpdf->AddPage();

        // background
        $mpdf->Image(FCPATH . "/assets/images/pdf-4.PNG", 3, 6, 210, 0, 'PNG', '', true, false);
        $mpdf->Image(FCPATH . "/assets/images/pdf-5.PNG", 3, 140, 210, 0, 'PNG', '', true, false);
        $mpdf->Image(FCPATH . "/assets/images/pdf-6.PNG", 3, 258, 210, 0, 'PNG', '', true, false);

        $mpdf->Output('Borang-Permohonan-SKIM-Bantuan-Baitulmal.pdf','I');
    }

    public function test()
    {
        $this->load->view('modul/pdf/design');
    }
}
