<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->view('login');
    }

    public function cek_login()
    {
        $username = $this->input->post('username');
        $password = md5($this->input->post('password'));

        $user = $this->mUser->get($username);
        if (empty($user)) {
            $this->session->set_flashdata('error', 'Username <b>not found !!</b>');
            redirect(base_url());
        } else {
            if ($password == $user->password) {
                $session = array(
                    'authenticated' => true,
                    'name' => $user->name,
                    'username' => $user->username,
                    'level' => $user->level
                );

                $this->session->set_flashdata('success', 'Welcome..');
                $this->session->set_userdata($session);
                redirect('dashboard');
            } else {
                $this->session->set_flashdata('error', '<b>Wrong</b> Password !!');
                redirect(base_url());
            }
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url());
    }
}
