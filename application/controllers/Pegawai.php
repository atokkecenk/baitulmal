<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pegawai extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->authenticated == false)
            redirect(base_url());
    }

    public function permohonan_skim()
    {
        $this->renderPage('modul/pegawai/permohonan_skim');
    }

    public function guru()
    {
        $this->renderPage('modul/pegawai/guru');
    }

    public function pelajar()
    {
        $this->renderPage('modul/pegawai/pelajar');
    }
}
