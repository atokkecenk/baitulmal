<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		if ($this->session->authenticated == false)
			redirect(base_url());
	}

	public function index()
	{
		$this->renderPage('modul/dashboard');
	}
}
