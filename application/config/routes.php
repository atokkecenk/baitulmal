<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Landing/index';
$route['dashboard'] = 'Home/index';

// PEGAWAI
$route['permohonan-skim'] = 'Pegawai/permohonan_skim';
$route['guru'] = 'Pegawai/guru';
$route['pelajar'] = 'Pegawai/pelajar';

$route['permohonan-skim-pdf'] = 'Mpdf/printPDF';
$route['design'] = 'Mpdf/test';

// AUTH
$route['auth'] = 'Auth/index';
$route['login'] = 'Auth/cek_login';
$route['logout'] = 'Auth/logout';

$route['404_override'] = '_404';
$route['translate_uri_dashes'] = FALSE;
