<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_menu extends CI_Model
{

    public function __mn_lev1($id_level)
    {
        $q = $this->db->query("SELECT
                                a.*,
                                b.parent,
                                b.name_menu,
                                b.route,
                                b.icon,
                                b.`order` 
                            FROM
                                tb_accessmenu a
                                LEFT JOIN tb_menu b ON a.id_menu = b.id_menu 
                            WHERE
                                a.id_level = '$id_level' 
                                AND b.parent = '0' 
                                AND a.na = '1' 
                                AND b.na = '1' 
                                AND b.na = '1' 
                            ORDER BY
                                b.`order` ASC");
        return $q;
    }

    public function __mn_lev2($id_level, $id_parent)
    {
        $q = $this->db->query("SELECT
                                a.*,
                                b.parent,
                                b.name_menu,
                                b.route,
                                b.icon,
                                b.`order` 
                            FROM
                                tb_accessmenu a
                                LEFT JOIN tb_menu b ON a.id_menu = b.id_menu 
                            WHERE
                                a.id_level = '$id_level' 
                                AND b.parent = '$id_parent' 
                                AND a.na = '1' 
                                AND b.na = '1' 
                            ORDER BY
                                b.`order` ASC");
        return $q;
    }
}
